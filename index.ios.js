/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { StackNavigator } from 'react-navigation';

import HomeScreen from './app/components/HomeScreen/home-screen';
import MovieScreen from './app/components/MovieScreen/movie-screen';

const ReactTheater= StackNavigator({
    Home: {screen: HomeScreen},
    Movie: {screen: MovieScreen},
});

AppRegistry.registerComponent('ReactTheater', () => ReactTheater);