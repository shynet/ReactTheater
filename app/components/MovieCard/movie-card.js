import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Image,
    Text,
    View,
    TextInput
} from 'react-native';

import Button from 'react-native-button';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';

export default class MovieCard extends Component {
    handler() {
        this.forceUpdate();
    }

    onLikeClick() {
        this.state.movie.likes += (this.state.movie.likedMovie) ? -1 : 1;
        this.state.movie.likedMovie = !this.state.movie.likedMovie;
        this.forceUpdate();
    }

    onCommentClick() {
        this.setState({ showComments : !this.state.showComments});
    }

    constructor(props) {
        super(props);
        this.state = {};
        this.state.showComments = false;
        this.state.movie = props.movie;

        if (props.showComments != undefined)
            this.state.showComments = props.showComments;

        this.handler = this.handler.bind(this);
    }

    render() {
        return (

            <View style={[styles.container, styles.card]}>
                <View style={{alignSelf: 'stretch', flexDirection: 'row', padding: 10}}>
                    <Image style={{height: 50, width: 50, borderRadius: 25}}
                           source={require('../../img/sarah-avatar.png.jpeg')}/>

                    <View style={{flexDirection: 'column', marginTop: 5, marginLeft: 15}}>
                        <Text style={styles.title}>{ this.state.movie.name }</Text>
                        <Text style={{color: 'gray'}}>{ moment(this.state.movie.releaseDate).format('DD/MM/YYYY') }</Text>
                    </View>
                </View>

                <View style={{alignItems: 'center'}}>
                    <Image style={{height: 220, resizeMode: 'center'}}
                           source={require('../../img/advance-card-bttf.png')}/>

                    <View style={{flexWrap: 'wrap', marginTop: 10, marginBottom: 20}}>

                        <Text style={{color: 'gray', fontSize: 14}}>
                            { this.state.movie.summary }
                        </Text>
                    </View>
                </View>

                <View style={{flexDirection: 'row', flex: 1}}>
                    <Button onPress={this.onLikeClick.bind(this)}>
                        <View style={{flexDirection: 'row'}}>
                            <Icon name="thumbs-up" size={18} color="#007aff" />
                            <Text style={{color: '#007aff', marginLeft: 5}}>{ this.state.movie.likes } Likes</Text>
                        </View>
                    </Button>

                    <View style={{width: 20}} />

                    <Button onPress={this.onCommentClick.bind(this)}>
                        <View style={{flexDirection: 'row'}}>
                            <Icon name="comment" size={18} color="#007aff" />
                            <Text style={{color: '#007aff', marginLeft: 5}}>{ this.state.movie.comments.length } Comments</Text>
                        </View>
                    </Button>
                </View>

                <Comments comments={this.state.movie.comments }
                          showComments={ this.state.showComments }
                          handler={this.handler} />
            </View>
        )
    }
}

class Comments extends Component {
    constructor() {
        super()
        this.state = { commentContent : "" };
    }

    onCommentEntered(event) {
        this.props.comments.push({ name : "Guest", content: this.state.commentContent});
        this.setState({ commentContent : "" });
        this.props.handler();
    }

    render() {
        if (!this.props.showComments)
            return null;

        return (
            <View style={{flexDirection: 'column'}}>
                {
                    this.props.comments.map((comment, index) => {
                        return (
                            <View key={index} style={{flexDirection: 'row', marginTop: 20}}>
                                <Image style={{height: 50, width: 50, borderRadius: 25}}
                                       source={require('../../img/avatar-ts-woody.png')}/>

                                <View style={{flexDirection: 'column', marginLeft: 15, marginTop: 3}}>
                                    <Text style={{fontSize: 18}}>{ comment.name }</Text>
                                    <Text style={{color: 'gray'}}>{ comment.content }</Text>
                                </View>
                            </View>
                        )
                    })
                }

                <TextInput placeholder="Leave a comment"
                           value={this.state.commentContent}
                           onChangeText={(text) => this.setState({commentContent : text})}
                           onSubmitEditing={this.onCommentEntered.bind(this)}
                           style={{height: 40, marginTop: 18}} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    card: {
        backgroundColor: "#fff",
        borderRadius: 2,
        shadowColor: "#000000",
        shadowOpacity: 0.3,
        shadowRadius: 1,
        shadowOffset: {
            height: 1,
            width: 0.3,
        },
        padding: 10,
        flexWrap: 'wrap'
    },
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
        margin: 5
    },
    title: {
        fontSize: 20,
        backgroundColor: 'transparent'
    },
    button: {
        marginRight: 10
    }
});