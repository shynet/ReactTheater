import React, {Component} from 'react';
import {ScrollView} from 'react-native';
import MovieCard from '../MovieCard/movie-card';

export default class MovieScreen extends Component {
    static navigationOptions = ({navigation}) => ({
        title: navigation.state.params.movie.name,
    });

    constructor(props) {
        super(props);
    }

    render() {
        const {params} = this.props.navigation.state;
        return (
            <ScrollView>
                <MovieCard movie={ params.movie } showComments={true}></MovieCard>
            </ScrollView>
        )
    }
}