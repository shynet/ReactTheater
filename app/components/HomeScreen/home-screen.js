/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    ListView,
    Text,
    View,
    TouchableOpacity
} from 'react-native';

import ServerUrl from '../../../constants';
import MovieCard from '../MovieCard/movie-card';
import MovieScreen from '../MovieScreen/movie-screen';
var localMovies = require('../../../local_db.json');

export default class HomeScreen extends Component {
    static navigationOptions = {
        title: 'New Releases',
    }

    constructor() {
        super();
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(localMovies),
        }
    }

    onMovieClick(movie) {
        this.props.navigation.navigate('Movie', {movie: movie});
    }

    render() {
        return (
            <ListView
                dataSource={this.state.dataSource}
                renderRow={(movie) =>
                    <TouchableOpacity onPress={this.onMovieClick.bind(this, movie)}>
                        <MovieCard movie={movie}></MovieCard>
                    </TouchableOpacity>
                }
            />
        );
    }
}